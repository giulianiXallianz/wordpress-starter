<?php
/***********************************************************************
********************Enqueue the parent theme stylesheet.********************
***********************************************************************/
function vantage_child_enqueue_parent_style() {
    wp_enqueue_style( ‘vantage-parent-style’, get_template_directory_uri() . ‘/style.css’);
}
add_action( ‘wp_enqueue_scripts’, ‘vantage_child_enqueue_parent_style’, 20);

/***********************************************************************
********************Enqueue custom stylesheets.****************************
***********************************************************************/
function my_custom_stylesheets() {
	wp_enqueue_style( 'parentStyle', get_stylesheet_directory_uri() . '/css/parentStyle.css' );
	wp_enqueue_style( 'myStyle', get_stylesheet_directory_uri() . '/style.css' );
}
add_action('wp_enqueue_scripts','my_custom_stylesheets');


/***********************************************************************
********************Enqueue custom javascript files.*************************
***********************************************************************/
function my_custom_javascript() {
    wp_enqueue_script( 'customJs', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), 1.0);
}
add_action('wp_enqueue_scripts','my_custom_javascript');

?>