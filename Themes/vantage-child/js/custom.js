/**
 * Created by l.giuliani on 21.10.2016.
 */

// ALL READY TO GO
jQuery(document).ready(function(){

    // fix tracking with "span" error
    jQuery('.ow-button-hover').each(function(){
       var _innerText = jQuery(this).find("span").html();
       jQuery(this).html(_innerText);
    });


});

// SCROLL TO A GIVEN ACNHOR ID JUST CALL WITH ID WHERE YOU WANT TO SCROLL TO
function scrollToAnchor(aid){
    var aTag = jQuery(aid);
    var MenuOffset = 30;
    jQuery('html,body').animate({scrollTop: aTag.offset().top - MenuOffset},'slow');
}