# README #

This is a repo with common plugins and 2 starter themes for quicker development

### How do I get set up? ###

* Download it, push everything in the plugin folder in your WP installation to plugin, and the 2 themes in your theme folder
* After that log into your wp installation and activate the vantage child theme and also all the plugins
* Roll updates as needed and start to develop 
