=== Head and Footer Scripts Inserter ===
Contributors: Arthur Gareginyan
Tags: inject, insert, insert scripts, insert javascript, insert js, insert html, insert css, insert custom scripts, insert custom code, html, javascript, js, css, code, custom code, script, scripts, custom scripts, meta, meta tags, head, header, head section, head area, footer, footer section, footer area,
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8A88KC7TFF6CS
Requires at least: 3.9
Tested up to: 4.7
Stable tag: 3.3
License: GPL3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Easily add your scripts to the WordPress website's head and footer sections. This is a must have tool for authors and website's owners.

== Description ==

An easy to use and lightweight WordPress plugin that gives you the ability to easily insert custom scripts (HTML, JavaScript, and CSS) in the head or/and footer section of your website.

No need anymore to editing a files of your theme or plugins in order to add custom scripts (HTML with JavaScript, CSS and else). You can add they on plugin's page. Just add your scripts in the field on the plugin's page and this plugin will do the rest for you. It adds required scripts to the head section of your website automatically, without changing any of your themes file and without slowing down your website. It's really useful in case of any theme update, because your scripts would never be lost! Your scripts will keep on working, no matter how many times you upgrade or switch your theme and plugins.

Third-party services like Google Webmaster Tools, Alexa, Pinterest and Google+ require you to verify your domain. This makes sure that you are the correct owner of your blog or store before they provide their services to you. You can use this plugin to easily verify your website or domain and get a more effective and efficient sharing results. Example with Pinterest. Once you completed the verification process, people will see a checkmark next to your domain in your Pinterest profile and in pinner search results. That check mark emphasis you have confirmed the ownership of your blog or website on Pinterest. This will help your website to rank better in google and other search engines. You can easily increase your blog traffic using this plugin. "Head and Footer Scripts Inserter" is a simple but effective SEO plugin.

= Features =

* Light weight
* User-friendly
* No configuration required
* Inserts scripts in beginning or/and end of <head> tag
* Inserts scripts in beginning or/and end of footer
* Ready for translation (POT file included)

= Translation =

Please keep in mind that not all translations are up to date. You are welcome to contribute!

* English (default)
* Russian
* Polish

= Supported =

* HTML
* JavaScript (in HTML tag)
* CSS (in HTML tag)

>**Contribution**
>
>Developing plugins is long and tedious work. If you benefit or enjoy this plugin please take the time to:
>
>* [Donate](http://www.arthurgareginyan.com/donate.html) to support ongoing development. Your contribution would be greatly appreciated.
>* [Rate and Review](https://wordpress.org/support/view/plugin-reviews/header-and-footer-scripts-inserter?rate=5#postform) this plugin.
>* [Share with me](mailto:arthurgareginyan@gmail.com) or view the [GitHub Repo](https://github.com/ArthurGareginyan/header-and-footer-scripts-inserter) if you have any ideas or suggestions to make this plugin better.


== Installation ==
Install "Head and Footer Scripts Inserter" just as you would any other WordPress Plugin.

Automatically via WordPress:

1. Log into Dashboard of your WordPress website.
2. Go to "`Plugins`" —> "`Add New`".
3. Find this plugin and click install.
4. Activate this plugin through the "`Plugins`" tab.

Manual via FTP:

1. Download a copy (ZIP file) of this plugin from WordPress.org.
2. Unzip the ZIP file.
3. Upload the unzipped catalog to your website's plugin directory (`/wp-content/plugins/`).
4. Log into Dashboard of your WordPress website.
5. Activate this plugin through the "`Plugins`" tab.

After installation, a "`Head and Footer Scripts Inserter`" menu item will appear in the "`Settings`" section. Click on this in order to view plugin's administration page.

[More help installing Plugins](http://codex.wordpress.org/Managing_Plugins#Installing_Plugins "WordPress Codex: Installing Plugins")


== Frequently Asked Questions ==
= Q. Will this plugin work on my WordPress.COM website? =
A. Sorry, this plugin is available for use only on self-hosted (WordPress.org) websites.

= Q. Can I use this plugin on my language? =
A. Yes. But If your language is not available then you can make one. This plugin is ready for translation. The `.pot` file is included and placed in "`languages`" folder. Many of plugin users would be delighted if you shared your translation with the community. Just send the translation files (`*.po, *.mo`) to me at the arthurgareginyan@gmail.com and I will include the translation within the next plugin update.

= Q. How does it work? =
A. First, go to page "`Settings`" —> "`Head and Footer Scripts Inserter`".
On the plugin's page you find the fields where you can enter your custom scripts.

= Q. How much of scripts I can enter in the field? =
A. I don't limit the number of characters.

= Q. Does this plugin require modification to the theme? =
A. Absolutely not. This plugin is added/configured entirely from the website's Admin section.

= Q. It's not working. What could be wrong? =
A. As with every plugin, it's possible that things don't work. The most common reason for this is that the plugin has a conflict with another plugin you're using. It's impossible to tell what could be wrong exactly, but if you post a support request in the plugin's support forum on WordPress.org, I'd be happy to give it a look and try to help out. Please include as much information as possible, including a link to your website where the problem can be seen.

= Q. Where to report bug if found? =
A. Please visit [Dedicated Plugin Page on GitHub](https://github.com/ArthurGareginyan/header-and-footer-scripts-inserter) and report.

= Q. Where to share any ideas or suggestions to make the plugin better? =
A. Please send me email [arthurgareginyan@gmail.com](mailto:arthurgareginyan@gmail.com).

= Q. I love this plugin! Can I help somehow? =
A. Yes, any financial contributions are welcome! Just visit my website and click on the donate link, and thank you! [My website](http://www.arthurgareginyan.com/donate.html)


== Screenshots ==
1. Plugin page.
2. Plugin page with Google Tag Manager code added.
3. Success message.

== Other Notes ==

"Head and Footer Scripts Inserter" is one of the personal software projects of [Arthur Gareginyan](http://www.arthurgareginyan.com).

**License**

This plugin is open-sourced software licensed under the [GNU General Public License, version 3 (GPLv3)](http://www.gnu.org/licenses/gpl-3.0.html) and is distributed free of charge.
Commercial licensing (e.g. for projects that can’t use an open-source license) is available upon request.

**Credits**

[CodeMirror](https://codemirror.net/) is an open-source project shared under the [MIT license](https://codemirror.net/LICENSE).

**Links**

* [Developer Website](http://www.arthurgareginyan.com)
* [Dedicated Plugin Page on GitHub](https://github.com/ArthurGareginyan/header-and-footer-scripts-inserter)

== Changelog ==
= 3.3 =
* Added the Readme.txt file for translation contribution.
* Added global constant for plugin text-domain.
* Translations updated.
* Ad banner replaced with new.
= 3.2 =
* Added prefixes to the stylesheet and script names when using wp_enqueue_style() and wp_enqueue_script().
* Added constant for storing the plugin version number.
= 3.1 =
* Style sheet of settings page improved and better commented.
* The "main.js" file renamed to "admin.js".
* JS code improved.
= 3.0.2 =
* Added improved section with using explanation to plugin settings page.
* admin.css improved.
= 3.0.1 =
* POT file updated.
* Russian translation updated.
* Polish translation updated.
* Image "thanks.png" removed.
* Advertisement replaced by new.
* Added the subject with plugin name to email address on settings page.
= 3.0 =
* Plugin renamed to "Head and Footer Scripts Inserter".
* Styles of settings page optimized for mobile devices.
* Added the syntax highlighting (by CodeMirror).
* Added line numbering.
* Added active-line add-on to CodeMirror.
* Removed the default message about successful saving.
* Added the custom message about successful saving.
* Added function of automatic remove the "successful" message after 3 seconds.
* Added function of automatic add the missing lines to get 10 lines.
* The _output function optimized.
* The "images" catalog renamed to "img".
* The CodeMirror library moved to "lib" catalog.
* The style.css file renamed to admin.css and moved to "css" catalog.
* The main.js moved to "js" catalog.
* The settings_page.php file moved to "php" catalog.
= 2.0 =
* Fixed: "Use of undefined constant header_beginning - assumed 'header_beginning' in settings_page.php".
* Fixed: "Use of undefined constant header_end - assumed 'header_end' in settings_page.php".
* Fixed: "Use of undefined constant footer_beginning - assumed 'footer_beginning' in settings_page.php".
* Fixed: "Use of undefined constant footer_end - assumed 'footer_end' in settings_page.php".
* Some changes in design of settings page.
* Constants variables added.
* Text domain changed to "header-and-footer-scripts-inserter".
* Added compatibility with the translate.wordpress.org.
* All images are moved to the directory "images".
* Image "btn_donateCC_LG.gif" is now located in the "images" directory.
* Plugin URI changed to GitHub repository.
* Added my personal ad about freelance.
* .pot file updated.
= 1.2 =
* Added Polish translation. (Thanks Paweł K.)
* Localization improved. (Thanks Paweł K.)
* POT file updated. (Thanks Paweł K.)
= 1.1 =
* Added Russian translation.
* Localization improved.
= 1.0 =
* Initial release.
* Added ready for translation (.pot file included).
= 0.2 =
* Beta version.
= 0.1 =
* Alfa version.


== Upgrade Notice ==
= 3.0 =
Please update to new release!
= 2.0 =
Please update to new release!
= 1.0 =
Please update to first stable release!
= 0.2 =
Please update to beta version.
