
CONTRIBUTING YOUR TRANSLATION

If you want to help translate this plugin, please visit the [translation page](https://translate.wordpress.org/projects/wp-plugins/header-and-footer-scripts-inserter).

Thanks for your contribution!