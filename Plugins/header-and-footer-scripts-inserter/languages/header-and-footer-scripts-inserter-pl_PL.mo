��          �   %   �      0     1     7     I  �   P     �      �                6     ;  c   H  b   �  q     k   �     �     �  @     O   P  F   �  D   �  R   ,       F   �  �  �  	   �
     �
     �
  U   �
     O  .   ^     �  &   �     �     �  h   �  k   =  q   �  n     
   �     �  @   �  ]   �  I   M  M   �  ^   �     D  F   d                   
                       	                                                                                     About Arthur Gareginyan Donate Easily add your scripts to the WordPress website's head and footer sections. This is a must have tool for authors and website's owners. Footer Section Got something to say? Need help? Head Section Head and Footer Scripts Inserter Help Save Changes Scripts from this field will be printed after all footers scripts. Do not place plain text in this! Scripts from this field will be printed before a footers scripts. Do not place plain text in this! Scripts from this field will be printed in the beginning of <b>HEAD</b> section. Do not place plain text in this! Scripts from this field will be printed in the end of <b>HEAD</b> section. Do not place plain text in this! Settings Thanks for your support! This plugin allows you to easily insert scripts in your website. To use, enter your custom scripts, then click "Save Changes". It's that simple! You can use the fields below to add scripts to Footer of your website. You can use the fields below to add scripts to Head of your website. by <a href="http://www.arthurgareginyan.com" target="_blank">Arthur Gareginyan</a> http://www.arthurgareginyan.com https://github.com/ArthurGareginyan/header-and-footer-scripts-inserter Project-Id-Version: Header and Footer Scripts Inserter
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-01 18:43+0300
PO-Revision-Date: 2016-11-01 18:43+0300
Last-Translator: psouu <koko887@wp.pl>
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.4
X-Poedit-Basepath: ..
X-Poedit-WPHeader: header-and-footer-scripts-inserter.php
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: pl_PL
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 O wtyczce Arthur Gareginyan Wesprzyj Łatwe dodawanie kodu skryptów do nagłówka i stopki strony opartej na WordPressie. Sekcja: Stopka Masz coś do powiedzenia? Potrzebujesz pomocy? Sekcja: Nagłówek Wstawianie kodu do nagłówka i stopki Pomoc Zapisz Skrypty z tego pola zostaną dodane za skryptami ze stopki. Nie należy tu umieszczać zwykłego tekstu! Skrypty z tego pola zostaną dodane przed skryptami ze stopki. Nie należy tu umieszczać zwykłego tekstu! Skrypty z tego pola zostaną dodane na początku sekcji <b>HEAD</b>. Nie należy tu umieszczać zwykłego tekstu! Skrypty z tego pola zostaną dodane na końcu sekcji <b>HEAD</b>. Nie należy tu umieszczać zwykłego tekstu! Ustawienia Dziękujemy za wsparcie! Ta wtyczka pozwala łatwo wstawić kod skryptów na stronę www. Aby użyć, wprowadź kod skryptów, a następnie kliknij przycisk "Zapisz". To takie proste! Pola poniżej można użyć, aby dodać kody skryptów do stopki witryny. Pola poniżej można użyć, aby dodać kody skryptów do nagłówka witryny. wykonana przez <a href="http://www.arthurgareginyan.com" target="_blank">Arthur Gareginyan</a> http://www.arthurgareginyan.com https://github.com/ArthurGareginyan/header-and-footer-scripts-inserter 